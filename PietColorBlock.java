public class PietColorBlock extends PietBlock {
  // Exit points
  private PietBlock Ul;
  private PietBlock Ur;
  private PietBlock Rl;
  private PietBlock Rr;
  private PietBlock Dl;
  private PietBlock Dr;
  private PietBlock Ll;
  private PietBlock Lr;
  // Exit point major/minor coords
  private int mx;
  private int Mx;
  private int my;
  private int My;
  private int mx_my;
  private int mx_My;
  private int Mx_my;
  private int Mx_My;
  private int my_mx;
  private int my_Mx;
  private int My_mx;
  private int My_Mx;

  public PietColorBlock( int x, int y, PietMatrix pm, PietVirtualMachine pvm, PietBlock[][] checked) {
    super(pm,pvm,checked);
    // Initialize the major/minor coords
    mx = my_mx = My_mx = PM.getWidth();
    my = mx_my = Mx_my = PM.getHeight();
    Mx = my_Mx = My_Mx = My = mx_My = Mx_My = -1;
    // Set block properties
    hue = PM.getHue(x,y);
    lightness = PM.getLightness(x,y);
    Ul = null;
    Ur = null;
    Rl = null;
    Rr = null;
    Dl = null;
    Dr = null;
    Ll = null;
    Lr = null;
    // Create the block
    createBlock(x,y);
    // Link the exits
    linkExits();
  }

  // Unfortunately modulo in Java doesn't work the way I want it to
  // -1 % 3 should be 2, but it ends up being -1, so I have to tweak it
  public void VMCommand( int prevVal, int prevHue, int prevLightness ) {
    if(prevVal == 0) { // only white blocks should have a val of zero
      PVM.runCmd(0,0,0);
    }
    else {
      int dhm = (hue-prevHue)%6;
      int dh = dhm >= 0 ? dhm : 6+dhm;
      int dlm = (lightness-prevLightness)%3;
      int dl = dlm >= 0 ? dlm : 3+dlm;
      PVM.runCmd(dh,dl,prevVal); // run command as specified in matrix in piet specs
    }
  }

  // Get the next block. 
  public PietBlock getNextBlock() {
    PietUtil.Dirs DP;
    PietUtil.Dirs CC;
    for(int i = 0; i < 8; i++) {
      DP = PVM.getDP();
      CC = PVM.getCC();
      if( DP == PietUtil.Dirs.Up && CC == PietUtil.Dirs.Left ) { if(stepNextBlock(Ul)) return Ul; }
      else if( DP == PietUtil.Dirs.Up && CC == PietUtil.Dirs.Right ) { if(stepNextBlock(Ur)) return Ur; }
      else if( DP == PietUtil.Dirs.Right && CC == PietUtil.Dirs.Left ) { if(stepNextBlock(Rl)) return Rl; }
      else if( DP == PietUtil.Dirs.Right && CC == PietUtil.Dirs.Right ) { if(stepNextBlock(Rr)) return Rr; }
      else if( DP == PietUtil.Dirs.Down && CC == PietUtil.Dirs.Left ) { if(stepNextBlock(Dl)) return Dl; }
      else if( DP == PietUtil.Dirs.Down && CC == PietUtil.Dirs.Right ) { if(stepNextBlock(Dr)) return Dr; }
      else if( DP == PietUtil.Dirs.Left && CC == PietUtil.Dirs.Left ) { if(stepNextBlock(Ll)) return Ll; }
      else if( DP == PietUtil.Dirs.Left && CC == PietUtil.Dirs.Right ) { if(stepNextBlock(Lr)) return Lr; }
      else return null;
    }
    return null;
  }

  // Recursively create a block.
  protected void createBlock(int x, int y) {
    if(x < 0 || y < 0 || x >= PM.getWidth() || y >= PM.getHeight()) return; // Out of the picture
    if(PM.getHue(x,y) != hue || PM.getLightness(x,y) != lightness) return; // Out of the block
    if(Checked[y][x] == this) return; // Already checked it
    Checked[y][x] = this; // Make the codel in Checked have a pointer to this block
    val++; // Increment size of block
    updatePossibleExits(x,y); // Update the possible exits
    // Recurse!
    createBlock(x-1,y);
    createBlock(x+1,y);
    createBlock(x,y-1);
    createBlock(x,y+1);
  }

  // Helper function for getNextBlock
  private boolean stepNextBlock(PietBlock check) {
    if(check == null) {
      if(f == 0) {
        f = 1;
        PVM.flipCC(1);
      }
      else {
        f = 0;
        PVM.rotateDP(1);
      }
      return false;
    }
    else {
      return true;
    }
  }

  // This updates the possible exits as we recursively find the contents of a block.
  // It was this or do several sorts with clamps on a matrix.
  private void updatePossibleExits(int x, int y) {
    if(x==mx) {
      if(y<mx_my) mx_my=y;
      if(y>mx_My) mx_My=y;
    }
    if(x==Mx) {
      if(y<Mx_my) Mx_my=y;
      if(y>Mx_My) Mx_My=y;
    }
    if(y==my) {
      if(x<my_mx) my_mx=x;
      if(x>my_Mx) my_Mx=x;
    }
    if(y==My) {
      if(x<My_mx) My_mx=x;
      if(x>My_Mx) My_Mx=x;
    }
    if(x<mx) { mx=x; mx_my=y; mx_My=y; }
    if(x>Mx) { Mx=x; Mx_my=y; Mx_My=y; }
    if(y<my) { my=y; my_mx=x; my_Mx=x; }
    if(y>My) { My=y; My_mx=x; My_Mx=x; }
  }

  // This links exits.
  private void linkExits() {
    if(mx-1 < 0) { Ll = null; Lr = null; } // Ll and Lr hit an edge
    else {
      Lr = linkExit(mx-1,mx_my,PietUtil.Dirs.Left,PietUtil.Dirs.Right);
      Ll = linkExit(mx-1,mx_My,PietUtil.Dirs.Left,PietUtil.Dirs.Left);
    }
    if(my-1 < 0) { Ul = null; Ur = null; } // Ul and Ur hit an edge
    else {
      Ul = linkExit(my_mx,my-1,PietUtil.Dirs.Up,PietUtil.Dirs.Left);
      Ur = linkExit(my_Mx,my-1,PietUtil.Dirs.Up,PietUtil.Dirs.Right);
    }
    if(Mx+1 >= PM.getWidth()) { Rl = null; Rr = null; } // Rl and Rr hit an edge
    else {
      Rl = linkExit(Mx+1,Mx_my,PietUtil.Dirs.Right,PietUtil.Dirs.Left);
      Rr = linkExit(Mx+1,Mx_My,PietUtil.Dirs.Right,PietUtil.Dirs.Right);
    }
    if(My+1 >= PM.getHeight()) { Dl = null; Dr = null; } // Dl and Dr hit an edge
    else {
      Dr = linkExit(My_mx,My+1,PietUtil.Dirs.Down,PietUtil.Dirs.Right);
      Dl = linkExit(My_Mx,My+1,PietUtil.Dirs.Down,PietUtil.Dirs.Left);
    }
  }

  private PietBlock linkExit(int x, int y, PietUtil.Dirs DP, PietUtil.Dirs CC)  {
    if(Checked[y][x] != null)
      return Checked[y][x];
    else if(PM.getHue(x,y) == -2)
      return null;
    else if(PM.getHue(x,y) == -1)
      return new PietWhiteBlock(x,y,DP,CC,PM,PVM,Checked);
    else
      return new PietColorBlock(x,y,PM,PVM,Checked);
  }
}

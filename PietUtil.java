public class PietUtil {
  public enum Dirs {
    Right { public Dirs rotate() { return Down;  }
            public Dirs flip  () { return Left;  }
          },
    Down  { public Dirs rotate() { return Left;  }
            public Dirs flip  () { return Down;  } // Down can't be flipped
          },
    Left  { public Dirs rotate() { return Up;    }
            public Dirs flip  () { return Right; }
          },
    Up    { public Dirs rotate() { return Right; }
            public Dirs flip  () { return Up;    } // Up can't be flipped
          };

    public abstract Dirs rotate();
    public abstract Dirs flip();
  }
}

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

class PietMain {
  private static final boolean _Benchmark_ = false;

  public static void Usage(String executable) {
    System.out.println("Usage: " + executable + " [-n count] [-r] image.png ");
    System.out.println("Arguments: -n count : Number of consecutive times to run image.png");
    System.out.println("           -r       : Include this flag to reparse image.png after each run");
  }

  public static void main(String[] args) {
    // Quickly did this, not pretty but it works 
    int n = 1;
    boolean r = false;
    String fname = "image.png";
    if(args.length == 4) {
      if(args[0].equals("-n")) {
        try { n = Integer.valueOf(args[1]); }
        catch (NumberFormatException e) { System.out.println(e); }
        r = true;
        fname = args[3];
      }
      else {
        Usage("PietMain");
        return;
      }
    }
    else if(args.length == 3) {
      if(args[0].equals("-n")) {
        try { n = Integer.valueOf(args[1]); }
        catch (NumberFormatException e) { System.out.println(e); }
        fname = args[2];
      }
      else {
        Usage("PietMain");
        return;
      }
    }
    else if(args.length == 1) {
      fname = args[0];
    }
    else {
      Usage("PietMain");
      return;
    }

    ThreadMXBean tb = ManagementFactory.getThreadMXBean();
    long start_t = tb.getCurrentThreadCpuTime();
    long start = System.nanoTime();
    PietGrammar PG = new PietGrammar(fname);
    for(int i = 0; i < n; i++) {
      while(PG.step()) {}
      PG.reset();
      System.out.println();
      if(r) PG = new PietGrammar(fname);
    }
    long end_t = tb.getCurrentThreadCpuTime();
    long end = System.nanoTime();
    if(_Benchmark_) System.out.println("Approximate average run time is " + (end-start)/n + "nanoseconds");
    if(_Benchmark_) System.out.println("Approximate average CPU time is " + (end_t-start_t)/n + "nanoseconds");
  }
}

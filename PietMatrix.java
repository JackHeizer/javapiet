import java.io.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.lang.Math.*;

public class PietMatrix {
  private int height;
  private int width;
  private int[][][] mHL;

  public PietMatrix( String fname ) {
    try{
      BufferedImage bimg = ImageIO.read(new File(fname));
      height = bimg.getHeight();
      width = bimg.getWidth();
      mHL = new int[height][width][2];
      for(int j = 0; j < height; j++) {
        for(int i = 0; i < width; i++) {
          int R = (bimg.getRGB(i,j) >> 16) & 0x000000FF;
          int G = (bimg.getRGB(i,j) >>  8) & 0x000000FF;
          int B = bimg.getRGB(i,j) & 0x000000FF;
          int maxCC = Math.max(R,Math.max(G,B));
          int minCC = Math.min(R,Math.min(G,B));
          mHL[j][i][0] = RGBtoHue(R,G,B,maxCC,minCC);
          mHL[j][i][1] = RGBtoLightness(maxCC,minCC);
        }
      }
    } catch (Exception e) { System.out.println(e.toString()); }
  }


  public int getHue(int x, int y) {
    if( x < 0 || x >= width || y < 0 || y >= height )
      return -10; // Random value for out of bounds... Will implement throw later
    return mHL[y][x][0];
  }

  public int getLightness(int x, int y) {
    if( x < 0 || x >= width || y < 0 || y >= height )
      return -10; // Random value for out of bounds... Will implement throw later 
    return mHL[y][x][1];
  }

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  // Simply uses the binrep function to determine the color.
  // Colors are 0-5 for normal colors, -1 for white and
  // -2 for black.
  private int RGBtoHue(int R, int G, int B, int max, int min) {
    // Check to see if it's black
    if( R == 0x00 && G == 0x00 && B == 0x00 )
      return -2;
    // Check to see if it's white
    if( R == 0xFF && G == 0xFF && B == 0xFF )
      return -1;
    // Check to see if it's any other color,
    // return white (-1) if it is.
    if( !((max == 0xFF || max == 0xC0 || max == 0x00) && (min == 0xFF || min == 0xC0 || min == 0x00)) )
      return -1;
    int rtn = -1;
    int b = RGBtoHueBinRep(R,G,B,max,min);
    switch(b) {
      case 1: rtn = 4; break;
      case 2: rtn = 2; break;
      case 3: rtn = 3; break;
      case 4: rtn = 0; break;
      case 5: rtn = 5; break;
      case 6: rtn = 1; break;
      // Shouldn't happen, but if it does return white (-1)
      //default: rtn = -1;
    }
    return rtn;
  }

  // Lightness is entirely determined by the max and min vals.
  // Lightness is 0-2 for normal colors, -1 for black and white.
  private int RGBtoLightness(int max, int min) {
    // Light color
    if( max == 0xFF && min == 0xC0 )
      return 0;
    // Regular color
    else if( max == 0xFF && min == 0x00 )
      return 1;
    // Dark color
    else if( max == 0xC0 && min == 0x00 )
      return 2;
    // Black, white, or anything not in the table
    else
      return -1;
  }

  // There's a pattern to the colors which is hard to explian.
  // Basically you have 0xRGB in hex. R, G, and B can be either
  // 0xFF, 0xC0, or 0x00. If you look at each column of the
  // specification you'll notice that if you take the max
  // and min values, if R, G, or B == max replace it with
  // a 1, if it equals the min replace it with a 0, then
  // Red == 100
  // Yellow == 110
  // Green == 010
  // Cyan == 011
  // Blue == 001
  // Magenta == 101
  // That is it doesn't matter if the color is light red,
  // red, or dark red, if you use this method the representation
  // will *always* be 100. So this is an easy way to figure out
  // the color, regardless of lightness, without a huge switch.
  // Although you can still use a massive switch/if-else statement
  // if you want ;).
  private int RGBtoHueBinRep(int R, int G, int B, int max, int min) {
    // Shift and bitwise-OR to get 0bXXX (0b is binary)
    // This will return 111 for both white and black
    // and chokes on any other color, we need to look at
    // that in the parent function.
    return (R==max?1:0)<<2 | (G==max?1:0)<<1 | (B==max?1:0);
  }

  /* --- Debug --- */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    for(int j = 0; j < height; j++) {
      for(int i = 0; i < width; i++) {
        String Clr;
        switch(getLightness(i,j)) {
          case 0: sb.append("L"); break;
          case 1: sb.append("N"); break;
          case 2: sb.append("D"); break;
        }
        switch(getHue(i,j)) {
          case -2: sb.append("Bl "); break;
          case -1: sb.append("WW "); break;
          case  0: sb.append("R "); break;
          case  1: sb.append("Y "); break;
          case  2: sb.append("G "); break;
          case  3: sb.append("C "); break;
          case  4: sb.append("B "); break;
          case  5: sb.append("M "); break;
        }
      }
      sb.append("\n");
    }
    return (new String(sb));
  }
}

public class PietWhiteBlock extends PietBlock {
  // Exit point
  private PietBlock Exit;
  // Temp CC and DP, after sliding through white block the real DP and CC should be equal to these
  private PietUtil.Dirs tDP;
  private PietUtil.Dirs tCC;

  public PietWhiteBlock( int x, int y, PietUtil.Dirs DP, PietUtil.Dirs CC, PietMatrix pm, PietVirtualMachine pvm, PietBlock[][] checked) {
    super(pm,pvm,checked);
    tDP = DP;
    tCC = CC;
    hue = -1;
    lightness = -1;
    Exit = null;
    createBlock(x,y);
  }

  public void VMCommand( int prevVal, int prevHue, int prevLightness ) {
    PVM.runCmd(0,0,0);
  }
 
  public PietBlock getNextBlock() {
    // We must set the real DP and CC to the proper value!
    while( tDP != PVM.getDP() ) PVM.rotateDP(1);
    if( tCC != PVM.getCC() ) PVM.flipCC(1);
    // Either Exit points to another block or it's null
    // If it's null we are trapped in white and must exit
    return Exit;
  }

  // In PietColorBlock this is a recursive function, here it is not. It would be impractical
  // to do so. When calling this function we *should* know the pixel at x,y is white. After
  // that we need to continuously check, but the first should be given.
  protected void createBlock(int x, int y) {
    int[][] tmask = new int[PM.getHeight()][PM.getWidth()];
    for(int i = 0; i < PM.getHeight(); i++) {
      for(int j = 0; j < PM.getWidth(); j++) {
        tmask[i][j] = 0;
      }
    }
    int cmask = getMask(tDP);
    int dx = 0; int dy = 0;
    while((tmask[y+dy][x+dx] & cmask) == 0) {
      tmask[y+dy][x+dx] |= cmask;
      if( tDP == PietUtil.Dirs.Left ) {
        if(checkNext(x+dx-1,y+dy)) dx -= 1;
      }
      else if( tDP == PietUtil.Dirs.Up ) {
        if(checkNext(x+dx,y+dy-1)) dy -= 1;
      }
      else if( tDP == PietUtil.Dirs.Right ) {
        if(checkNext(x+dx+1,y+dy)) dx += 1;
      }
      else if( tDP == PietUtil.Dirs.Down ) {
        if(checkNext(x+dx,y+dy+1)) dy += 1;
      }
      else Exit = null; // Shouldn't happen
      if( Exit != null ) return; // we found it somewhere up there ^
          cmask = getMask(tDP);
    }
    Exit = null; // Sets to null and returns only if we start retracing our path
  }

  // Returns false if the next move puts us out of bounds or into a black
  // block. Returns true if we continue into another white codel or we
  // find a colored codel. If we find a colored codel we actually link it
  // to differentiate the two.
  private boolean checkNext(int x, int y) {
    if( x < 0 || y < 0 || x >= PM.getWidth() || y >= PM.getHeight() ) {
      flipAndRotate();
      return false;
    }
    else if(PM.getHue(x,y) == -2) {
      flipAndRotate();
      return false;
    }
    else if(PM.getHue(x,y) == -1)
      return true;
    else {
      linkExit(x,y);
      return true;
    }
  }

  // Link the exit
  // Shorter than the color version since it can't be white
  // and it can't be black. So it either exists or it's a
  // new color block
  private void linkExit(int x, int y) {
    if(Checked[y][x] != null)
      Exit = Checked[y][x];
    else
      Exit = new PietColorBlock(x,y,PM,PVM,Checked);
  }

  // Get direction bitmask
  private int getMask(PietUtil.Dirs DP) {
    if(DP == PietUtil.Dirs.Left) return 0x1;
    if(DP == PietUtil.Dirs.Down) return 0x2;
    if(DP == PietUtil.Dirs.Right) return 0x4;
    if(DP == PietUtil.Dirs.Up) return 0x8;
    return 0x0; // Shouldn't happen...
  }

  private void flipAndRotate() {
    tDP = tDP.rotate();
    tCC = tCC.flip();
  }
}

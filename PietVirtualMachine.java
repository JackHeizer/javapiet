import java.util.LinkedList;
import java.io.*;
import java.util.Iterator;

public class PietVirtualMachine {
  // Commands. Kind of like a function pointer array
  enum Commands {
    nop        { void exec(int val) { 
//                                      System.out.println("action: nop");
                                    }
               },
    push       { void exec(int val) {
//                                      System.out.println("action: push " + val);
                                      s.addLast(val);
                                    }
               },
    pop        { void exec(int val) {
//                                      System.out.println("action: pop");
                                      if(s.size() >= 1)
                                        s.removeLast();
                                    }
               },
    add        { void exec(int val) {
//                                      System.out.println("action: add");
                                      if(s.size() >= 2) 
                                      {
                                        int rrand = s.removeLast();
                                        int lrand = s.removeLast();
                                        s.addLast(lrand+rrand);
                                      }
                                    }
               },
    subtract   { void exec(int val) {
//                                      System.out.println("action: subtract");
                                      if(s.size() >= 2) 
                                      {
                                        int rrand = s.removeLast();
                                        int lrand = s.removeLast();
                                        s.addLast(lrand-rrand);
                                      }
                                    }
               },
    multiply   { void exec(int val) {
//                                      System.out.println("action: multiply");
                                      if(s.size() >= 2)
                                      {
                                        int rrand = s.removeLast();
                                        int lrand = s.removeLast();
                                        s.addLast(lrand*rrand);
                                      }
                                    }
               },
    divide     { void exec(int val) {
//                                      System.out.println("action: divide");
                                      if(s.size() >= 2)
                                      {
                                        int rrand = s.removeLast();
                                        int lrand = s.removeLast();
                                        s.addLast(lrand/rrand);
                                      }
                                    }
               },
    mod        { void exec(int val) {
//                                      System.out.println("action: mod");
                                      if(s.size() >= 2)
                                      {
                                        int rrand = s.removeLast();
                                        int lrand = s.removeLast();
                                        s.addLast(lrand%rrand);
                                      }
                                    }
               },
    complement { void exec(int val) {
//                                      System.out.println("action: complement");
                                      if(s.size() >= 1)
                                      {
                                        int rand = s.removeLast();
                                        s.addLast( rand != 0 ? 0 : 1 );
                                      }
                                    }
               },
    greater    { void exec(int val) {
//                                      System.out.println("action: greater");
                                      if(s.size() >= 2)
                                      {
                                        int rrand = s.removeLast();
                                        int lrand = s.removeLast();
                                        s.addLast(lrand>rrand ? 1 : 0 );
                                      }
                                    }
               },
    pointer    { void exec(int val) {
//                                      System.out.println("action: pointer");
                                      if(s.size() >= 1)
                                      {
                                        int rand = s.removeLast();
                                        rotateDP(rand);
                                      }
                                    }
               },
    flip       { void exec(int val) {
//                                      System.out.println("action: flip");
                                      if(s.size() >= 1)
                                      {
                                        int rand = s.removeLast();
                                        flipCC(rand);
                                      }
                                    }
               },
    duplicate  { void exec(int val) {
//                                      System.out.println("action: duplicate");
                                      if(s.size() >= 1)
                                        s.addLast(s.getLast());
                                    }
               },
    roll       { void exec(int val) {
//                                      System.out.println("action: roll");
                                      if(s.size() >= 2) {
                                        int rolls = s.removeLast();
                                        int depth = s.removeLast();
                                        rolls %= depth;
                                        rolls = (rolls >= 0) ? rolls : rolls+depth;
                                        if( (depth > 0) && (rolls > 0) && (s.size() > 0) ) {
                                          Iterator<Integer> it = s.listIterator(s.size()-depth);
                                          for(int i = 0; i < rolls; i++) {
                                            s.add(s.size()-depth,s.removeLast());
                                          }
                                        }
                                      }
                                    }
               },
    ini        { void exec(int val) {
//                                      System.out.println("action: in(int)");
                                        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                                        char v;
                                        try { 
                                              v = (char)(br.read()); 
                                              s.addLast(Integer.parseInt(v + ""));
                                            }
                                        catch (IOException e) { System.out.println(e); }
                                    //      s.addLast(31);
                                    }
               },
    inc        { void exec(int val) {
//                                      System.out.println("action: in(char)");
                                        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                                        int v;
                                        try { 
                                              v = br.read(); 
                                              s.addLast(v);
                                            }
                                        catch (IOException e) { System.out.println(e); }
                                    }
               },
    outi       { void exec(int val) {
//                                      System.out.println("action: out(int)");
                                      if(s.size() >= 1)
                                      {
                                        int rand = s.removeLast();
                                        System.out.print(rand);
                                      }
                                    }
               },
    outc       { void exec(int val) {
//                                      System.out.println("action: out(char)");
                                      if(s.size() >= 1)
                                      {
                                        char rand = (char)(s.removeLast().intValue());
                                        System.out.print(rand);
                                      }
                                    }
               };

    abstract void exec(int val);
  }

  // Vars
  private static PietUtil.Dirs DP;
  private static PietUtil.Dirs CC;
  private static LinkedList<Integer> s;

  // Constructor
  public PietVirtualMachine() {
    DP = PietUtil.Dirs.Right;
    CC = PietUtil.Dirs.Left;
    s = new LinkedList<Integer>();
  }

  // VM State
  // Java enum is even worse...
  public static void rotateDP(int steps) { 
    int n = (steps%4) >= 0 ? steps%4 : 4+(steps%4);
    for(int i = 0; i < n; i++)
      DP = DP.rotate();
  }

  public PietUtil.Dirs getDP() { return DP; }

  public static void flipCC(int steps) { 
    int n = (steps%2) >= 0 ? steps%2 : 2+(steps%2);
    if(n == 1) CC = CC.flip();
  }

  public PietUtil.Dirs getCC() { return CC; }

  public void runCmd(int dh, int dl, int v) { Commands.values()[dh*3+dl].exec(v); }

  public void reset() {
    s.clear();
    DP = PietUtil.Dirs.Right;
    CC = PietUtil.Dirs.Left;
  }
}

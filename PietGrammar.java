// The "grammar" is made up of color blocks
// Each block has a positive integer value (n > 0)
// Each block also has a numerical color value and hue value
// The value, hue, and color determine the command to be run
// This is more or less a shell class to construct and link
// all of the PietBlocks.
public class PietGrammar {
  private PietMatrix PM;
  private PietVirtualMachine PVM;
  private PietBlock[][] Checked;
  private PietBlock firstBlock;
  private PietBlock curBlock;
  private PietBlock nextBlock;

  public PietGrammar( String fName ) {
    PM = new PietMatrix(fName);
    PVM = new PietVirtualMachine();
    Checked = new PietBlock[PM.getHeight()][PM.getWidth()]; 
    if(PM.getHue(0,0) == -1)
      firstBlock = new PietWhiteBlock(0,0,PietUtil.Dirs.Right,PietUtil.Dirs.Left,PM,PVM,Checked);
    else if(PM.getHue(0,0) == -2)
      firstBlock = null;
    else
      firstBlock = new PietColorBlock(0,0,PM,PVM,Checked);
    curBlock = firstBlock;
  }

  // Do a single step. 
  // Returns true if it executed a command, otherwise false.
  // It will only return false when the program flow is trapped and can't leave
  // the current block.
  public boolean step() {
    if(curBlock == null) return false; // First block was black!
    nextBlock = curBlock.getNextBlock();
    if(nextBlock == null) return false;
    nextBlock.VMCommand(curBlock.getVal(), curBlock.getHue(), curBlock.getLightness());
    curBlock = nextBlock;
    return true;
  }

  // Reset for another run without re-parsing the image
  public void reset() {
    PVM.reset();
    curBlock = firstBlock;
  }
}

public abstract class PietBlock {
  protected PietVirtualMachine PVM; // The virtual machine
  protected PietMatrix PM; // The actual image data
  protected PietBlock[][] Checked; // Maintains a matrix of codels, if a block has been established all of its codels will contain the object's pointer (this in java I guess...)
  protected int val; // number of codels (pixels at this point)
  protected int hue; // 0,1,2,3,4,5
  protected int lightness; // 0,1,2
  protected int f; // To determine when to flip and when to rotate the CC and DP respectively

  public PietBlock( PietMatrix pm, PietVirtualMachine pvm, PietBlock[][] checked ) {
    PM = pm;
    PVM = pvm;
    Checked = checked;
    val = 0;
    f = 0;
  }

  public abstract void VMCommand( int prevVal, int prevHue, int prevLightness );
  public abstract PietBlock getNextBlock();
  protected abstract void createBlock( int x, int y );

  public int getVal() {
    return val;
  }

  public int getHue() {
    return hue;
  }

  public int getLightness() {
    return lightness;
  }
}
